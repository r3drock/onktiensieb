package createTable

import (
	"database/sql"
	"github.com/lib/pq"
	"reflect"
	"unsafe"
)

func filterDuplicateTableError (err error) error {
	if reflect.TypeOf(err) == reflect.TypeOf(&pq.Error{}) {
		pErr := (*pq.Error)(unsafe.Pointer(reflect.ValueOf(err).Pointer()))
		if pErr.Code.Name() == "duplicate_table" {
			return nil
		}
	}
	return err
}

func Companies(db *sql.DB) {
	query := `create TABLE companies( "TICKER" TEXT,
				"MIC" text,
				PRIMARY KEY("TICKER", "MIC"));`
	_, err := db.Exec(query)
	err = filterDuplicateTableError(err)
	if err != nil {
		panic(err)
	}
}

func Mics(db *sql.DB) {
	query := `create TABLE mics( "MIC" text,
				PRIMARY KEY("MIC"));`
	_, err := db.Exec(query)
	err = filterDuplicateTableError(err)
	if err != nil {
		panic(err)
	}
}

func Companyyears(db *sql.DB) {
	query := `create TABLE companyyears( "TICKER" TEXT,
				"MIC" text,
				"PERIODENDDATE" date,
				"NET INCOME" money,
				"DEPRECIATION & AMORTIZATION" money,
                "OTHER NON CASH CHARGES" money,
				"CHANGE IN WORKING CAPITAL" money,
				"CAPITAL EXPENDITURE" money,
				"INVESTMENTS IN PROPERTY, PLANT, AND EQUIPMENT" money,
				"STOCK BASED COMPENSATION" money,
				"OTHER NON-CASH ITEMS" money,
				"INVESTMENT/ASSET IMPAIRMENT CHARGES" money,
				"SHORT-TERM INVESTMENTS" money,
				"TOTAL CURRENT ASSETS" money,
				"TOTAL CURRENT LIABILITIES" money,
				PRIMARY KEY("TICKER", "MIC", "PERIODENDDATE"));`
	_, err := db.Exec(query)
	err = filterDuplicateTableError(err)
	if err != nil {
		panic(err)
	}
}

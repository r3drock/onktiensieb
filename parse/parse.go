package parse

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"math"
	"onktiensieb/apirequest"
	"time"
)

type responseStatusType struct {
	ResponseStatus string
}
type lineItem struct {
	Label string
	Value float64
}
type subsection struct {
	SectionHeader   string
	LineItems       []lineItem
	Subsections     []subsection
	SummaryLineItem lineItem
}
type section struct {
	SectionHeader   string
	LineItems       []lineItem
	Subsections     []subsection
	SummaryLineItem lineItem
}
type result struct {
	IsTrailing12Months bool
	Sections           []section
	PeriodEndDate      string
}
type Statement struct {
	Total          int
	Offset         int
	Results        []result
	ResponseStatus responseStatusType
}

type exchangeList struct {
	Total int
	Offset int
	Results []exchangeCompany
	ResponseStatus responseStatusType
}

type exchangeCompany struct {
	Mic string
	Currency string
	Ticker string
	SecurityId string
	EndOfDayQuoteTicker string
	CompanyName string
}

type companyFinancialData struct {
	PeriodEndDate time.Time
	Data          map[string]float64
}

type flatList struct {
	Total int
	Offset int
	Results []string
	ResponseStatus responseStatusType
}

func Mics(db *sql.DB) {
	rawjson := apirequest.AllMics()
	var list flatList
	err := json.Unmarshal(rawjson, &list)
	if err != nil {
		panic(err)
	}
	for _, mic := range list.Results {
		query := fmt.Sprintf(`INSERT INTO mics ("MIC")
					VALUES ('%s');`,
			mic)
		_, err = db.Exec(query)
		if err != nil {
			panic(err)
		}
	}
}

func CompaniesOnExchange(db *sql.DB, mic string) {
	rawjson := apirequest.AllCompaniesOnExchange(mic)
	var list exchangeList
	err := json.Unmarshal(rawjson, &list)
	if err != nil {
		panic(err)
	}
	for _, company := range list.Results {
		query := fmt.Sprintf(`INSERT INTO companies ("TICKER",
					"MIC")
					VALUES ('%s', '%s');`,
			company.Ticker,
			company.Mic)
		_, err = db.Exec(query)
		if err != nil {
			fmt.Println(err, company.Ticker, company.Mic)
		}
	}
}

func balanceSheet(years []companyFinancialData, balanceSheet *Statement) {
	for i, _ := range years {
		years[i].Data = make(map[string]float64)
	}
	var err error
	for i, result := range balanceSheet.Results {
		years[i].PeriodEndDate, err = time.Parse("2006-01-02", result.PeriodEndDate)
		if err != nil {
			panic(err)
		}
		for _, section := range result.Sections {
			for _, subsection := range section.Subsections {
				for _, lineItem := range subsection.LineItems {
					years[i].Data[lineItem.Label] = lineItem.Value
				}
				if subsection.SummaryLineItem.Label != "" {
					if _, ok := years[i].Data[subsection.SummaryLineItem.Label]; ok {
					} else {
						years[i].Data[subsection.SummaryLineItem.Label] = subsection.SummaryLineItem.Value
					}
				}
				for _, subsubsection := range subsection.Subsections {
					for _, lineItem := range subsubsection.LineItems {
						years[i].Data[lineItem.Label] = lineItem.Value
					}
					if subsubsection.SummaryLineItem.Label != "" {
						if _, ok := years[i].Data[subsubsection.SummaryLineItem.Label]; ok {
						} else {
							years[i].Data[subsubsection.SummaryLineItem.Label] = subsubsection.SummaryLineItem.Value
						}
					}
				}
			}
		}
	}
}

func cashFlowStatement(years []companyFinancialData, cashFlowStatement *Statement) {
	for i, _ := range years {
		years[i].Data = make(map[string]float64)
	}
	var err error
	for i, result := range cashFlowStatement.Results {
		years[i].PeriodEndDate, err = time.Parse("2006-01-02", result.PeriodEndDate)
		if err != nil {
			panic(err)
		}
		for _, section := range result.Sections {
			for _, subsection := range section.Subsections {
				for _, lineItem := range subsection.LineItems {
					years[i].Data[lineItem.Label] = lineItem.Value
				}
				if subsection.SummaryLineItem.Label != "" {
					if _, ok := years[i].Data[subsection.SummaryLineItem.Label]; ok {
					} else {
						years[i].Data[subsection.SummaryLineItem.Label] = subsection.SummaryLineItem.Value
					}
				}
			}
		}
	}
}

func incomeStatement(years []companyFinancialData, incomeStatement *Statement) {
	for i, _ := range years {
		years[i].Data = make(map[string]float64)
	}
	var err error
	for i, result := range incomeStatement.Results {
		years[i].PeriodEndDate, err = time.Parse("2006-01-02", result.PeriodEndDate)
		if err != nil {
			panic(err)
		}
		for _, section := range result.Sections {
			for _, lineItem := range section.LineItems {
				years[i].Data[lineItem.Label] = lineItem.Value
			}
			for _, subsection := range section.Subsections {
				if subsection.SummaryLineItem.Label != "" {
					if _, ok := years[i].Data[subsection.SummaryLineItem.Label]; ok {
					} else {
						years[i].Data[subsection.SectionHeader+subsection.SummaryLineItem.Label] = subsection.SummaryLineItem.Value
					}
				}
			}
		}
	}
}

func AllCompanyYears(db *sql.DB, mic string) {
	query := `SELECT "TICKER" 
				FROM companies
				WHERE "MIC"=$1;`
	rows, err := db.Query(query, mic)
	if err != nil {
		panic(err)
	}
	for i := 0; rows.Next(); i++ {
		if i > 100 {
			break;
		}
		var ticker []byte
		err = rows.Scan(&ticker)
		if err != nil {
			panic(err)
		}
		var tickerS = string(ticker)

		CompanyYears(db, mic, tickerS)
	}
}

func CompanyYears(db *sql.DB, mic string, ticker string) {
	var err error
	//var incomeStatement Statement
	//e = json.Unmarshal(getTicker("income-statement", ticker , mic), &incomeStatement)
	//var incomeYears = make([]companyFinancialData, len(incomeStatement.Results), len(incomeStatement.Results))
	//parse.IncomeStatement(incomeYears, &incomeStatement)

	var balSheet Statement
	err = json.Unmarshal(apirequest.Ticker("balance-sheet", mic, ticker), &balSheet)
	if err != nil {
		panic(err)
	}
	var balanceSheetYears = make([]companyFinancialData, len(balSheet.Results), len(balSheet.Results))
	balanceSheet(balanceSheetYears, &balSheet)

	var cashStatement Statement
	err = json.Unmarshal(apirequest.Ticker("cashflow-statement", mic, ticker), &cashStatement)
	if err != nil {
		panic(err)
	}
	var cashFlowYears = make([]companyFinancialData, len(cashStatement.Results), len(cashStatement.Results))
	cashFlowStatement(cashFlowYears, &cashStatement)
	//for i, _ := range cashFlowYears {
	//	fmt.Println(cashFlowYears[i], "\n")
	//	fmt.Println(balanceSheetYears[i], "\n")
	//	//fmt.Println(incomeYears[i], "\n\n============================\n")
	//}

	for i, _ := range cashFlowYears {
		query := fmt.Sprintf(`INSERT INTO companyyears ("TICKER",
					"MIC",
					"PERIODENDDATE",
					"NET INCOME",
					"DEPRECIATION & AMORTIZATION",
					"CHANGE IN WORKING CAPITAL",
					"CAPITAL EXPENDITURE",
					"INVESTMENTS IN PROPERTY, PLANT, AND EQUIPMENT",
					"STOCK BASED COMPENSATION",
					"OTHER NON-CASH ITEMS",
					"INVESTMENT/ASSET IMPAIRMENT CHARGES",
					"SHORT-TERM INVESTMENTS",
					"TOTAL CURRENT ASSETS",
					"TOTAL CURRENT LIABILITIES")
					VALUES ('%s', '%s', '%s', %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f);`,
			ticker,
			mic,
			cashFlowYears[i].PeriodEndDate.Format("2006-01-02"),
			cashFlowYears[i].Data["NET INCOME"],
			cashFlowYears[i].Data["DEPRECIATION & AMORTIZATION"],
			cashFlowYears[i].Data["CHANGE IN WORKING CAPITAL"],
			cashFlowYears[i].Data["CAPITAL EXPENDITURE"],
			cashFlowYears[i].Data["INVESTMENTS IN PROPERTY, PLANT, AND EQUIPMENT"],
			cashFlowYears[i].Data["STOCK BASED COMPENSATION"],
			cashFlowYears[i].Data["OTHER NON-CASH ITEMS"],
			cashFlowYears[i].Data["INVESTMENT/ASSET IMPAIRMENT CHARGES"],
			balanceSheetYears[i].Data["SHORT-TERM INVESTMENTS"],
			balanceSheetYears[i].Data["TOTAL CURRENT ASSETS"],
			balanceSheetYears[i].Data["TOTAL CURRENT LIABILITIES"])
		//fmt.Println(query)
		_, err = db.Exec(query)
		if err != nil {
			//panic(e)
		}
	}
}


func Money(money []byte) float64 {
	var sign float64
	first := 2
	if money[0] == '-' {
		sign = -1
	} else {
		sign = 1
		first = 1
	}
	var amount float64
	for _, c := range money[first:] {
		if c == '.' {
			break
		}
		if c == ',' {
			continue
		}
		amount *=10
		amount += float64(c - '0')
	}
	return sign * amount
}



func ConvertMoney(amount float64) string {
	if amount == 0 {
		return`$0.00`
	}
	digits := int64(math.Log10(math.Abs(amount)))+1
	commas := (digits - 1) / 3

	len :=  digits + commas + 5
	money := make ([]byte, len, len)
	if amount >= 0 {
		money[0] = ' '
	} else {
		money[0] = '-'
		amount = math.Abs(amount)
	}
	money[1] = '$'
	i := len - 1
	money[i] = byte(uint(amount * 100)%10 + '0')
	money[i-1] = byte(uint(amount * 10)%10 + '0')
	money[i-2] = '.'
	i -= 3
	counter := 0
	intAmount := int(amount)
	for intAmount >= 1 {
		if counter == 3 {
			money[i] = ','
			counter = 0;
		} else {
			money[i] = byte(intAmount%10)+'0'
			intAmount /=10
			counter++
		}
		i--
	}
	return string(money[0:len-7])
}

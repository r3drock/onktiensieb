package apirequest

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"
)

func readFromCache(requestType string, mic string, ticker string) []byte {
	f, e := ioutil.ReadFile(requestType + "/" + mic + "/"+ ticker + ".json")
	if e != nil {
		return []byte("")
	}
	return f
}

func writeToCache(requestType string, mic string, ticker string, json []byte) {
	filename := requestType + "/" + mic + "/" + ticker + ".json"
	f, e := os.Create(filename)
	if os.IsNotExist(e) {
		e = os.Mkdir(requestType, 0755)
		if os.IsExist(e) {

		} else if e != nil {
			panic(e)
		}
		f, e = os.Create(filename)
	}
	if os.IsNotExist(e) {
		e = os.Mkdir(requestType + "/" + mic, 0755)
		if e != nil {
			panic(e)
		}
		f, e = os.Create(filename)
	}
	if e != nil {
		panic(e)
	}
	_, e = f.Write(json)
	if e != nil {
		panic(e)
	}
	e = f.Close()
	if e != nil {
		panic(e)
	}
}
var lastAPIRequestTime = time.Now()
func apiRequestJSON(url string) []byte {
	timeSinceLastAPIRequest := time.Now().Sub(lastAPIRequestTime).Milliseconds()
	if timeSinceLastAPIRequest < 500 {
		time.Sleep(time.Until(lastAPIRequestTime.Add(time.Millisecond * 500)))
	}
	lastAPIRequestTime = time.Now()
	fmt.Printf("API request for: %s at %s\n", url, lastAPIRequestTime)

	req, e := http.NewRequest("GET", url, nil)
	if e != nil {
		panic(e)
	}
	req.Header.Add("accept", "string")
	req.Header.Add("x-rapidapi-key", "41cf13f479msh3bbd8a35dc3b03fp1633e6jsn09303b407533")
	req.Header.Add("x-rapidapi-host", "morningstar1.p.rapidapi.com")

	res, e := http.DefaultClient.Do(req)
	if e != nil {
		panic(e)
	}
	defer res.Body.Close()
	rawJson, e := ioutil.ReadAll(res.Body)
	if e != nil {
		panic(e)
	}
	if string(rawJson) == `{"message":"You have exceeded the rate limit` {
		panic(rawJson)
	}
	return rawJson
}

func makeCachedRequest(url string, requestType string, mic string, ticker string) []byte {
	rawJson := readFromCache(requestType, mic, ticker)
	if len(rawJson) == 0 {
		rawJson = apiRequestJSON(url)
		writeToCache(requestType, mic, ticker, rawJson)
	}
	return rawJson
}

func Ticker(requestType string, mic string, ticker string) []byte {
	url := "https://morningstar1.p.rapidapi.com/fundamentals/yearly/" +
		requestType +
		"/restated?Mic=" + strings.ToUpper(mic) +
		"&Ticker=" + strings.ToUpper(ticker)
	return makeCachedRequest(url, requestType, mic, ticker)
}


func AllMics() []byte {
	url := "https://morningstar1.p.rapidapi.com/exchanges/" +
		"list-mics?Mic=XNAS"
	return makeCachedRequest(url, "mics", "all", "list")
}

func AllCompaniesOnExchange(mic string) []byte {
	url := "https://morningstar1.p.rapidapi.com/companies/" +
		"list-by-exchange?Mic=" + strings.ToUpper(mic)
	return makeCachedRequest(url, "companies", mic, "list")
}


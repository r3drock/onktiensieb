package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"html/template"
	"log"
	"math"
	"net/http"
	"os"
	"regexp"
	"onktiensieb/createTable"
	"onktiensieb/parse"
	"strconv"
	"strings"
	"sync"
	"time"
)

type dbConnData struct {
	host   string
	port     int
	user     string
	password string
	dbname   string
}

func connectToDatabase(c dbConnData) *sql.DB {
	connstr := fmt.Sprintf("user='%s' password='%s' dbname='%s' host='%s' port='%d' sslmode='disable'",
		c.user, c.password, c.dbname, c.host, c.port,
	)
	db, err := sql.Open("postgres", connstr)
	if err != nil {
		panic(err)
	}
	return db
}

func calcOwnersEarnings(db *sql.DB, mic string, ticker string, date time.Time) (float64,error) {
	query := `SELECT ("NET INCOME" + "DEPRECIATION & AMORTIZATION" +
				"STOCK BASED COMPENSATION" +  "OTHER NON-CASH ITEMS" + "INVESTMENT/ASSET IMPAIRMENT CHARGES"+
				"CAPITAL EXPENDITURE" + "CHANGE IN WORKING CAPITAL")::money::numeric::float8 AS "OWNERS EARNINGS"  
			  	FROM companyyears WHERE "MIC"=$1 AND "TICKER"=$2 AND date_part('year',"PERIODENDDATE")=$3;`
	row := db.QueryRow(query, mic, ticker, date.Year())
	var oE []byte
	err := row.Scan(&oE)
	if err != nil {
		return 0,err
	}
	ownersEarnings, err := strconv.ParseFloat(string(oE), 64)
	if err != nil {
		return 0, err;
	}
	return ownersEarnings, nil
}

func findFirstExchange(db *sql.DB, ticker string) []byte {
	query := `SELECT "MIC" 
				FROM companies
				WHERE "TICKER"=$1;`
	row := db.QueryRow(query, ticker)
	var mic []byte
	err := row.Scan(&mic)
	if err != nil {
		panic(err)
	}
	return mic
}

type stock struct {
	value  float64
	ticker string
	mic string
}

type yearStock struct {
	value  float64
	year string
}

func calcLast5ForTicker(db *sql.DB, mic string, ticker string, year int, f func(db *sql.DB, mic string, ticker string, date time.Time) float64) []yearStock {
	stocks := make([]yearStock, 0, 5)
	var sum float64

	query := `SELECT ("NET INCOME" + "DEPRECIATION & AMORTIZATION" +
		"STOCK BASED COMPENSATION" +  "OTHER NON-CASH ITEMS" + "INVESTMENT/ASSET IMPAIRMENT CHARGES" +
		"CAPITAL EXPENDITURE" +"CHANGE IN WORKING CAPITAL")::money::numeric::float8 AS "OWNERS EARNINGS"
	FROM companyyears WHERE "MIC"=$1 AND "TICKER"=$2 AND date_part('year',"PERIODENDDATE") BETWEEN $3 AND $4;`

	rows, err := db.Query(query, mic, ticker, year - 4, year)
	if err != nil {
		panic(err)
	}
	for y := year -4; rows.Next(); y++ {
		var oE []byte
		err = rows.Scan(&oE)
		if err != nil {
			panic(err)
		}
		ownersEarnings, err := strconv.ParseFloat(string(oE), 64)
		if err != nil {
			panic(err)
		}
		sum += ownersEarnings
		stocks = append(stocks, yearStock{
			value:  ownersEarnings,
			year: strconv.Itoa(y),
		})
	}
	stocks = append(stocks, yearStock{
		value:  sum/5,
		year: "average",
	})
	return stocks
}

var queryCacheMutex *sync.Mutex
var queryCache map[string]map[time.Time][]stock
func calcOwnersEarningsForMicCached(db *sql.DB, mic string, date time.Time) []stock {
	queryCacheMutex.Lock()
	if _, ok := queryCache[mic]; ok {} else {
		queryCache[mic] = make(map[time.Time][]stock)
	}
	if _, ok := queryCache[mic][date]; ok {} else {
		queryCacheMutex.Unlock()
		oE := calcOwnersEarningsForMic(db, mic, date)
		queryCacheMutex.Lock()
		queryCache[mic][date] = oE
		queryCacheMutex.Unlock()
		return oE
	}
	oE := queryCache[mic][date]
	queryCacheMutex.Unlock()
	return oE
}

func calcOwnersEarningsForMic(db *sql.DB, mic string, date time.Time) []stock {
	query := `SELECT "TICKER", ("NET INCOME" + "DEPRECIATION & AMORTIZATION" +
		"STOCK BASED COMPENSATION" +  "OTHER NON-CASH ITEMS" + "INVESTMENT/ASSET IMPAIRMENT CHARGES" +
		"CAPITAL EXPENDITURE" +"CHANGE IN WORKING CAPITAL")::money::numeric::float8 AS "OWNERS EARNINGS"
	FROM companyyears WHERE "MIC"=$1 AND date_part('year',"PERIODENDDATE")=$2;`
	stocks := make([]stock, 0, 512)
	rows, err := db.Query(query, mic, date.Year())
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		var ticker, oE []byte
		err = rows.Scan(&ticker, &oE)
		if err != nil {
			fmt.Println("could not scan row!\n")
			continue
		}
		ownersEarnings, err := strconv.ParseFloat(string(oE), 64)
		if err != nil {
			fmt.Println("parseError: could not parse %s\n", oE)
			continue
		}
		stocks = append(stocks, stock{
			value:  ownersEarnings,
			ticker: string(ticker),
			mic:    mic,
		})
	}
	return stocks
}

func generateTable(stocks []stock) template.HTML {
	var tableBuilder strings.Builder
	tableBuilder.WriteString("<table><tr><th>Ticker</th><th>Owner's Earnings</th></tr>\n")
	for _, s := range stocks {
		row := fmt.Sprintf("<tr><td><a href=\"/single/%s/%s\">%s</a></td><td>%s</td></tr>\n", s.mic, s.ticker, s.ticker, parse.ConvertMoney(s.value))
		tableBuilder.WriteString(row)
	}
	tableBuilder.WriteString("</table>\n")
	return template.HTML(tableBuilder.String())
}

var ownersEarningsCache map[string]map[string]map[int]float64
var mutex *sync.Mutex

func calcOwnersEarningsCached(db *sql.DB, mic string, ticker string, date time.Time) float64 {
	mutex.Lock()
	defer mutex.Unlock()
	if _, ok := ownersEarningsCache[mic]; ok {} else {
		ownersEarningsCache[mic] = make(map[string]map[int]float64)
	}
	if _, ok := ownersEarningsCache[mic][ticker]; ok {} else {
		ownersEarningsCache[mic][ticker] = make(map[int]float64)
	}
	var ownersEarning float64
	year := date.Year()
	if val, ok := ownersEarningsCache[mic][ticker][year]; ok {
		ownersEarning = val
	} else {
		var err error
		mutex.Unlock()
		ownersEarning, err = calcOwnersEarnings(db, mic, ticker, date)
		mutex.Lock()
		if err != nil {
			ownersEarningsCache[mic][ticker][year] = math.NaN()
			return  math.NaN()
		}
		ownersEarningsCache[mic][ticker][year] = ownersEarning
	}
	return ownersEarning
}

func calcHighestYear(db *sql.DB, mic string, ticker string) (time.Time, error) {
	query := fmt.Sprintf(`select MAX("PERIODENDDATE")
from companyyears where "MIC"=$1 and "TICKER"=$2 group by "TICKER";`)
	row := db.QueryRow(query, mic, ticker)
	var periodEndDate []byte
	err := row.Scan(&periodEndDate)
	if err != nil {
		date := time.Date(2020, 1, 1, 1, 1, 1, 1, time.UTC)
		return date,err
	}
	date, err := time.Parse(time.RFC3339, string(periodEndDate))
	if err != nil {
		date := time.Date(2020, 1, 1, 1, 1, 1, 1, time.UTC)
		return date,err
	}
	return date,err
}

var templates = template.Must(template.ParseFiles("templates/layout.html", "templates/layout_single.html", "templates/index.html"))

func renderTemplate(w http.ResponseWriter, templateFileName string, l exchangeList) {
	err := templates.ExecuteTemplate(w, templateFileName + ".html", l)
	if err != nil {
		log.Println("template error: ", templateFileName+".html")
		log.Println(templates.DefinedTemplates())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}



var validMicYearPath = regexp.MustCompile("^/list/([a-zA-Z]+)/([0-9]+)$")
var validMicTickerPath = regexp.MustCompile("^/single/([a-zA-Z]+)/([a-zA-Z]+)$")

func handleSingleTicker(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	m := validMicTickerPath.FindStringSubmatch(r.URL.Path)
	if len(m) != 3 {
		http.NotFound(w, r)
		return
	}
	mic := strings.ToUpper(m[1])
	ticker := strings.ToUpper(m[2])
	date, err := calcHighestYear(db, mic, ticker)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	stocks := calcLast5ForTicker(db, mic, ticker, date.Year(), calcOwnersEarningsCached)
	var tableBuilder strings.Builder
	tableBuilder.WriteString("<table><tr><th>Year</th><th>Owner's Earnings</th></tr>\n")
	for _, s := range stocks {
		row := fmt.Sprintf("<tr><td>%s</td><td>%s</td></tr>\n", s.year, parse.ConvertMoney(s.value))
		tableBuilder.WriteString(row)
	}
	tableBuilder.WriteString("</table>\n")
	table := template.HTML(tableBuilder.String())

	err = templates.ExecuteTemplate(w, "layout_single.html", yearList{
		Company: ticker,
		Table: table,
		Mic:     mic,
		Year: date.Year(),
	})
	if err != nil {
		log.Println("template error: ", "layout_single.html")
		log.Println(templates.DefinedTemplates())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}


type yearList struct {
	Company string
	Table template.HTML
	Mic string
	Year int
}
type exchangeList struct {
	LinkPrevYear template.HTML
	LinkNextYear template.HTML
	Table template.HTML
	Mic string
	Year int
}

func handler(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	m := validMicYearPath.FindStringSubmatch(r.URL.Path)
	if len(m) != 3 {
		http.NotFound(w, r)
		return
	}
	mic := strings.ToUpper(m[1])
	year, err := strconv.Atoi(m[2])
	if err != nil {
		log.Fatal("IMPOSSIBLE", err)
	}
	date := time.Date(year, 1, 1, 1, 1, 1, 1, time.UTC)
	table := generateTable(calcOwnersEarningsForMicCached(db, mic, date))
	renderTemplate(w,"layout", exchangeList{
		LinkPrevYear: template.HTML("<a rel=\"prefetch\" href=" + strconv.Itoa(year - 1) + ">" + "Previous Year</a>"),
		LinkNextYear: template.HTML("<a rel=\"prefetch\" href=" + strconv.Itoa(year + 1) + ">" + "Next Year</a>"),
		Table: table,
		Mic:     mic,
		Year:   year,
	})
}

func serveIndex(w http.ResponseWriter, r *http.Request, db *sql.DB) {
	query := `SELECT "MIC" 
				FROM mics;`
	rows, err := db.Query(query)
	if err != nil {
		panic(err)
	}
	var linkBuilder strings.Builder
	for i := 0; rows.Next(); i++ {
		var mic []byte
		err = rows.Scan(&mic)
		if err != nil {
			panic(err)
		}
		row := fmt.Sprintf("<p><a href=\"list/%s/2020\">%s</a></p>\n", mic, mic)
		linkBuilder.WriteString(row)
	}
	err = templates.ExecuteTemplate(w, "index.html", template.HTML(linkBuilder.String()))
	if err != nil {
		log.Println("template error: ", "index.html")
		log.Println(templates.DefinedTemplates())
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func serveStatic(w http.ResponseWriter, r *http.Request) {
	dir := http.Dir(".")
	file := r.URL.EscapedPath()
	f, err := dir.Open(file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	defer f.Close()
	fi, err := f.Stat()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	http.ServeContent(w, r, file, fi.ModTime(), f)
}

func makeHandler(handler func (http.ResponseWriter, *http.Request, *sql.DB), db *sql.DB) http.HandlerFunc {
	return func (w http.ResponseWriter, r *http.Request) {
		//start := time.Now()
		handler(w, r, db);
		//stop := time.Now()
		//fmt.Println("request took ", stop.Sub(start))
	}
}

func main() {
	ownersEarningsCache = make(map[string]map[string]map[int]float64)
	queryCache = make(map[string]map[time.Time][]stock)
	mutex = &sync.Mutex{}
	queryCacheMutex = &sync.Mutex{}

	db := connectToDatabase(
		dbConnData{
			host     : os.Getenv("POSTGRES_HOST"),
			port     : 5432,
			user     : os.Getenv("POSTGRES_USER"),
			password : os.Getenv("POSTGRES_PASSWORD"),
			dbname   : os.Getenv("POSTGRES_DB"),
		})
	defer db.Close()
	//createMicsTable(db)
	createTable.Companies(db)
	createTable.Mics(db)
	createTable.Companyyears(db)
	//createcompanyyearsTable(db)
	//insertIntoDB(db, ticker, cashFlowYears, balanceSheetYears)
	//parse.CompaniesOnExchange(db, "XNAS")
	//parse.Mics(db)
	mic := "XETR"
	parse.CompanyYears(db, "XNAS", "MSFT")
	parse.CompanyYears(db, "XNAS", "EBAY")
	parse.CompanyYears(db, "XNAS", "AAPL")
	parse.AllCompanyYears(db, mic)
	http.HandleFunc("/list/", makeHandler(handler, db))
	http.HandleFunc("/single/", makeHandler(handleSingleTicker, db))
	http.HandleFunc("/index.html", makeHandler(serveIndex, db))
	http.HandleFunc("/css/", serveStatic)
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) { return })
	log.Fatal(http.ListenAndServe("localhost:8000", nil))
}
